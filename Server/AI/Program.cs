﻿using Apache.Ignite.Core;
using Apache.Ignite.Core.Binary;
using Apache.Ignite.Core.Compute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI
{
    class Program
    {
        static void Main(string[] args)
        {
            var cfg = new IgniteConfiguration { BinaryConfiguration = new BinaryConfiguration(typeof(CountFunc)) };

            using(var ignite = Ignition.Start(cfg))
            {
                Console.Read();
            }            
        }
    }

    internal class CountFunc : IComputeFunc<int[], int>
    {
        public int Invoke(int[] arg)
        {
            int max_el = 0;
            string arr = "";

            for (int i = 0; i < arg.Length; i++)
            {
                if (arg[i] > max_el) {
                    max_el = arg[i];
                }
                arr += arg[i] + " ";
            }
            Console.WriteLine(arr + " Наибольший элемент в строке " + max_el);

            return max_el;
        }
    }
}
